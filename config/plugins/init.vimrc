"Setup imports for general config
" plug definitions should be first - do not sort with below
runtime ./config/plugins/plug_definitions.vimrc

runtime ./config/plugins/airline.vimrc
runtime ./config/plugins/ctrlp.vimrc
runtime ./config/plugins/markdown_preview.vimrc
runtime ./config/plugins/rg.vimrc
runtime ./config/plugins/swoop.vimrc
" was killing my computer everytime I saved a test file with ctms tests.
" need to manually trigger and not from buffer write or save
"runtime ./config/plugins/vim_test.vimrc
