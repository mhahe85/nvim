"Remap the markdown preview hotkey to <leader>mp to avoid conflict with Ctrl-P plugin
let vim_markdown_preview_hotkey='<Leader>mp'
let vim_markdown_preview_pandoc=1
