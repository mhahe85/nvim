" Swoop

let g:swoopUseDefaultKeyMap = 0

nnoremap <Leader>ss :call Swoop()<CR>
vnoremap <Leader>ss :call SwoopSelection()<CR>

nnoremap <Leader>sm :call SwoopMulti()<CR>
vnoremap <Leader>sm :call SwoopMultiSelection()<CR>

"" Swoop
"let g:swoopUseDefaultKeyMap = 0
"nnoremap <Leader>ss :call Swoop()<CR>
"vnoremap <Leader>ss :call SwoopSelection()<CR>
"nnoremap <Leader>sm :call SwoopMulti()<CR>
"vnoremap <Leader>sm :call SwoopMultiSelection()<CR>

