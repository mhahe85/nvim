"Setup imports for general config

" init.vimrc needs to be first -- do not sort with below
runtime ./config/plugins/init.vimrc

runtime ./config/appearance.vimrc
runtime ./config/general.vimrc
runtime ./config/keys.vimrc
runtime ./config/plugins.vimrc
runtime ./config/plugins/lsp.vimrc
